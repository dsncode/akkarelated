package sample.cluster.simple

import com.typesafe.config.ConfigFactory

import akka.actor.ActorSystem
import akka.actor.Props
import akka.cluster.Cluster

object SimpleClusterApp {
  
  def main(args:Array[String]):Unit ={
  
    var ports : Seq[String] = null;
    
    if(args.isEmpty)
      ports = Seq("2551");
    else
      ports = args;
    
    ports foreach { port =>
      println(s"Got port ${port}")
      // Override the configuration of the port
      val config = 
        ConfigFactory
        .parseString("akka.remote.netty.tcp.port=" + port).
        withFallback(ConfigFactory.load())
      
      // Create an Akka system
      val system = ActorSystem("ClusterSystem", config)
//      Cluster(system).join(akka.actor.AddressFromURIString.apply("akka.tcp://ClusterSystem@127.0.0.1:2001"))
      // Create an actor that handles cluster domain events
      system.actorOf(Props[SimpleClusterListener], name = "clusterListener")
  }
  }
}
