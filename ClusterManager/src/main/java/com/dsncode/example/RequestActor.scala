package com.dsncode.example

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Status

class RequestActor extends Actor with ActorLogging{
  
  val map = scala.collection.mutable.Map[String,Object]()
  
  override def receive = {
        case SetRequest(key, value) =>
        log.info("received SetRequest - key: {} value: {}", key, value)
        map.put(key, value)
        sender() ! Status.Success
        case GetRequest(key) =>
        log.info("received GetRequest - key: {}", key)
        val response: Option[Object] = map.get(key)
        response match{
        case Some(x) => sender() ! x
        case None => sender() ! Status.Failure(new
        KeyNotFoundException(key))
        }
        case o => Status.Failure(new ClassNotFoundException)
    }
  
}