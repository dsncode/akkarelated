package com.dsncode.ner.actor

import akka.actor.Actor
import java.util.regex.Pattern
import com.fasterxml.jackson.databind.ObjectMapper
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import akka.pattern.ask
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import akka.actor.ActorLogging

object RegexActor {
  val om = new ObjectMapper()
  case class RegRequest(text:String, pat : Pattern, classification : String)
  case class RegBuilder(tokens: Seq[String], language : String)
  
  
}

class RegexActor extends Actor with ActorLogging{
  import RegexActor._
//  implicit val timeout = Timeout(5 seconds)
//  val waittime = Duration.apply(5000, TimeUnit.MILLISECONDS)
  
  def receive={
    
    case RegRequest(text,pat,classification) =>{ 
      log.info("processing... pat: "+pat.toString().size)
      val m = pat.matcher(text);
      val arr = ArrayBuffer.empty[String]
      while(m.find())
      {
        val token = text.substring(m.start(), m.end())  
        arr+=token
      }
    val map = arr.groupBy(l=>l).mapValues(_.length)
    val response = om.createArrayNode();
    
    map.map(m=>{
      val node = om.createObjectNode()
      node.put("token", m._1)
      node.put("count", m._2)
      node.put("ner", classification)
      })
      .foldLeft(response)((arr,n1)=>{arr.add(n1);arr})
     sender ! response.toString()
    }
    
  }
  
}