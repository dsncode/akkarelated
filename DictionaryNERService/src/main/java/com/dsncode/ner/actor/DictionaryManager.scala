package com.dsncode.ner.actor

import java.util.concurrent.TimeUnit

import scala.collection.JavaConversions.asScalaIterator
import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.collection.JavaConverters.seqAsJavaListConverter
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.typesafe.config.ConfigFactory

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.dispatch.Futures
import akka.pattern.ask
import akka.util.Timeout
import akka.routing.ConsistentHashingGroup
import akka.cluster.routing.ClusterRouterGroupSettings
import akka.cluster.routing.ClusterRouterGroup
import akka.routing.ConsistentHashingRouter.ConsistentHashableEnvelope
import akka.cluster.routing.ClusterRouterPoolSettings
import akka.routing.ConsistentHashingPool
import akka.cluster.routing.ClusterRouterPool

class DictionaryManager(val sysname:String) {
  val om = new ObjectMapper();
  implicit val timeout = Timeout(60 seconds)
  val waittime = Duration.apply(60, TimeUnit.SECONDS)
  
    val config = 
        ConfigFactory
        .parseString("akka.remote.netty.tcp.port=2551")
        .withFallback(
          ConfigFactory.parseString("akka.cluster.roles = [leader]")).
        withFallback(ConfigFactory.load())
        
        
    val system = ActorSystem(sysname,config);  
  
    system.actorOf(Props[SimpleClusterListener], name = "clusterListener")
    system.actorOf(Props[RegexActor], name = "regexWorker")
    
    val regexRouter = system.actorOf(
  ClusterRouterPool(ConsistentHashingPool(0), ClusterRouterPoolSettings(
    totalInstances = 100, maxInstancesPerNode = 3,
    allowLocalRoutees = true, useRole = None)).props(Props[RegexActor]),name = "regexRouter")
    
//    val reverseRouter = system.actorSelection(akka.actor.ActorPath.fromString("/reverseWorker"));
    val reverseRouter = system.actorOf(
  ClusterRouterGroup(ConsistentHashingGroup(Nil), ClusterRouterGroupSettings(
    totalInstances = 100, routeesPaths = List("/user/reverseWorker"),
    allowLocalRoutees = true, useRole = Some("compute"))).props(),name = "reverseRouter")
    
    val map:scala.collection.mutable.Map[String,ActorRef] = scala.collection.mutable.Map[String,ActorRef]()
  
    def attachDictionary(dictionary_path : String, name : String, lang: String,local:Boolean)={
      val actor = system.actorOf(Props(new DictionaryActor(dictionary_path,name,lang,local)),name+"Actor")
      map+=(name->actor)
      actor.path.toString()
    }
  
    def shutdown=system.terminate()
    
    def reverse(txt:String)=reverseRouter?ConsistentHashableEnvelope(txt,txt.hashCode())
    
    def extract(txt:String)={
      
      val response =map.par.map(m=>{m._2 ? DictionaryActor.ExtractCommand(txt)})
      .toList.asJava
      
      val all_futures = Futures.sequence(response,global)
      val final_result = Await.result(all_futures, waittime)
      
      val arr = om.createArrayNode()
      
      final_result.toSeq
      .map{_.asInstanceOf[ArrayNode]}
      .flatMap { arr => arr.iterator().seq }
      .filter{!_.isEmpty}
      .foldLeft(arr)((arr,n1)=>{arr.add(n1);arr})
      
      arr
    }
    
    
    
}