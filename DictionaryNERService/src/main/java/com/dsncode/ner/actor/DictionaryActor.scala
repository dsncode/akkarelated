package com.dsncode.ner.actor

import java.io.File
import java.util.regex.Pattern

import org.apache.commons.io.FilenameUtils
import org.apache.log4j.Logger

import akka.actor.Actor
import scala.collection.parallel.immutable.ParSeq

import akka.pattern.ask
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import akka.dispatch.Futures
import ExecutionContext.Implicits.global
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import com.fasterxml.jackson.databind.node.ObjectNode
import com.dsncode.ner.actor.Dictionary;
import com.dsncode.ner.actor.RegexActor;
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import akka.actor.ActorLogging
import akka.routing.ActorRefRoutee
import akka.routing.RoundRobinRoutingLogic
import akka.actor.Props
import akka.routing.Router
import akka.actor.ActorRef
import akka.routing.RoundRobinPool
import akka.routing.ConsistentHashingGroup
import akka.cluster.routing.ClusterRouterGroupSettings
import akka.cluster.routing.ClusterRouterGroup
import akka.routing.ConsistentHashingRouter.ConsistentHashableEnvelope


object DictionaryActor {
  case class ExtractCommand(text : String)
  case class ReverseCommand(text:String)
  
  implicit val timeout = Timeout(60 seconds)
  val waittime = Duration.apply(60, TimeUnit.SECONDS)
  
  val om = new ObjectMapper();
}

class Dictionary(val name : String, val lang:String, val classification : String, val pattenr : ParSeq[Pattern])

class DictionaryActor(dictionary_path : String, name : String, lang: String,local:Boolean) extends Actor with ActorLogging{
  import DictionaryActor._
//  val regexActor = context.system.actorSelection("/user/regexActor")
  var dictionary : Dictionary = null;
  
//  val regexRouter: ActorRef = context.actorOf(RoundRobinPool(50).props(Props[RegexActor]), "regRouter")
  
  val regexRouter = context.actorSelection("/user/regexRouter")

  def getLines(path: String, local:Boolean)={
    if(!local)
      scala.io.Source.fromFile(dictionary_path)("UTF-8").getLines().toStream.seq
    else
        scala.io.Source.fromURL(getClass.getResource(path)).getLines().toStream.seq
  }
  
  
  override def preStart()={
    log.info("Initializing DictionaryActor... "+lang+" -> "+dictionary_path)
    val start = System.currentTimeMillis()
    
    val file = new File(dictionary_path)
    val lines = getLines(dictionary_path,local)
    
    
    val pattern_group = lines.grouped(50).map { group=>
//      log.info("parsing..."+group.size)
      var str_pat : String = null;
      
      if(lang.equalsIgnoreCase("english"))
        str_pat = group.seq.map { x => Pattern.quote(x) }.mkString("\\b(","|",")\\b")
      else
        str_pat = group.seq.map { x => Pattern.quote(x) }.mkString("(","|",")")
      
      var patt : Pattern=null;
      try
      {
        patt = Pattern.compile(str_pat, Pattern.CASE_INSENSITIVE)
      }catch{
        case _ => log.info("ERROR "+str_pat)
      }
      
      patt
    }.toStream.par;
    
      val classfication = FilenameUtils.getBaseName(file.getAbsolutePath()).toUpperCase();
      val end = System.currentTimeMillis()
      dictionary = new Dictionary(name,lang,classfication,pattern_group)
      log.info("DictionaryActor ["+classfication+"] terms:"+lines.size+" into "+pattern_group.size+" lines. initialized "+(end-start)+"ms")
  }
  
  
  def receive={
    
    
  
    case ExtractCommand(text) =>{
      log.info("processing text ")
      val response = dictionary.pattenr.map { pat => 
        val msg = RegexActor.RegRequest(text,pat,dictionary.classification)
        
        val f= regexRouter ? ConsistentHashableEnvelope(msg,msg.hashCode())  
        f
      }.map { x => x }.toList.asJava
      val re = Futures.sequence(response,global)
      val ok = Await.result(re, waittime)
      
      val arr = om.createArrayNode()
      
      ok.toSeq
      .map{r=>om.readTree(r.toString())}
      .flatMap { arr => arr.iterator().seq }
      .filter{!_.isEmpty}
      .foldLeft(arr)((arr,n1)=>{arr.add(n1);arr})
      
      sender ! arr
    }
 
    
  }  
  
}