package com.dsncode.ner.main;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.dsncode.ms.app.ApplicationManager;

@SpringBootApplication
@ComponentScan(basePackages = {"com.dsncode.ner.controller"})
public class DictionaryServiceMain {
	private static Logger log = Logger.getLogger(DictionaryServiceMain.class);
	public static void main(String[] args) throws InterruptedException {
//		System.setProperty("spring.config.name", "micro");
		ApplicationManager.start(DictionaryServiceMain.class,2000);
//		Thread.sleep(10000);
//		log.info("stopping...");
//		ApplicationManager.stop();
		
	}

}