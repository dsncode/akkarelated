package com.dsncode.ner.controller;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dsncode.ms.app.ApplicationManager;
import com.dsncode.ner.actor.DictionaryManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/dictionary")
public class NERController {

		private static ObjectMapper om = new ObjectMapper();
		private static DictionaryManager manager = new DictionaryManager("NerCluster");
		{
			try {
				ArrayNode defaults = (ArrayNode)om.readTree(ClassLoader.getSystemResource("ner-dic-config.json"));
			    StreamSupport.stream(defaults.spliterator(), false).forEach(j->{
			    	manager.attachDictionary(j.path("path").asText(), j.path("name").asText(), j.path("language").asText(), j.path("local").asBoolean(false));
			    });
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		
		@RequestMapping(value = "/version", method = RequestMethod.GET)
		public JsonNode getCustomers() {
			ObjectNode response = om.createObjectNode();
			response.put("app", "DictionaryNER");
			response.put("api version", "1.0");
			return response;
		}
		
		@RequestMapping(value="/load",method=RequestMethod.POST)
		public JsonNode load(@RequestBody JsonNode request )
		{
			manager.attachDictionary(request.path("path").asText(), request.path("name").asText(), request.path("lang").asText(),false);
			return om.createObjectNode().put("success", true);
		}
		
		@RequestMapping(value="/reverse",method=RequestMethod.POST)
		public JsonNode reverse(@RequestBody JsonNode request )
		{
			manager.reverse(request.path("text").asText());
			return om.createObjectNode().put("sent", true);
		}
		
		
		@RequestMapping(value="/extract",method=RequestMethod.POST)
		public JsonNode extract(@RequestBody JsonNode request ){
			ArrayNode response = manager.extract(request.path("text").asText());
			return response;
		}
		
		
//		@RequestMapping(value="/terminate",method=RequestMethod.DELETE)
//		public JsonNode terminate(){
//			Runnable termination_call = ()->{
//				manager.shutdown();
//			};
//
//			ApplicationManager.stop(4000);
//			ScheduledExecutorService scheduledThreadPool = Executors.newSingleThreadScheduledExecutor();
//			scheduledThreadPool.schedule(termination_call, 2, TimeUnit.SECONDS);
//			
//			return om.createObjectNode().put("destroy", "scheduled").put("time", 5);
//			
//		}
}
