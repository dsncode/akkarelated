package com.dsncode.ner.actor

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.actor.Props
import akka.cluster.Cluster

object WorkerMain{
  
  def main(args:Array[String]):Unit=
  {
      val cluster_address = "172.16.3.54:2551"//args(0)
      println(s"connecting to ${cluster_address}")
      val config =ConfigFactory.parseString(s"akka.remote.netty.tcp.port=2552").withFallback(
              ConfigFactory.parseString("akka.cluster.roles = [compute]"))
              .withFallback(ConfigFactory.load())

      val system = ActorSystem("NerCluster", config)

      val c = Cluster(system)
      c.join(akka.actor.AddressFromURIString.apply(s"akka.tcp://NerCluster@${cluster_address}"))
      system.actorOf(Props[RegexActor], name = "regexWorker")
      val reverse = system.actorOf(Props[ReverseTextActor], name = "reverseWorker")
      println("reverse actor path: ->"+reverse.path)
      system.actorOf(Props[RegexService], name = "regexService")
  }
}