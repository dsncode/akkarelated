package com.dsncode.ner.actor

import akka.actor.ActorLogging
import akka.actor.Actor
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.UnreachableMember
import akka.cluster.ClusterEvent.MemberEvent

class ReverseTextActor extends Actor with ActorLogging{

      val cluster = Cluster(context.system)
  override def preStart(): Unit = {
         log.info("creating actor...."+self.hashCode())
         cluster.subscribe(self, classOf[MemberEvent], classOf[UnreachableMember])
      }
  override def postStop(): Unit = cluster.unsubscribe(self)
  
  def receive={
    case text:String =>log.info(s"${text} -> ${text.reverse}");sender()!text.reverse
  }
  
  
}