package com.dsncode.ner.actor

import akka.actor.Props
import akka.routing.FromConfig
import akka.actor.ActorLogging
import akka.actor.Actor
import scala.concurrent.duration._
import akka.actor.ActorRef
import akka.actor.ReceiveTimeout
import akka.routing.ConsistentHashingRouter.ConsistentHashableEnvelope
import akka.routing.FromConfig
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt
import akka.dispatch.Futures
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import java.util.concurrent.TimeUnit
import com.fasterxml.jackson.databind.node.ArrayNode

class RegexService extends Actor with ActorLogging{
  
  val workerRouter = context.actorOf(Props[RegexActor],name = "regexActor")
  implicit val timeout = Timeout(60 seconds)
  val waittime = Duration.apply(60, TimeUnit.SECONDS)
  
  def receive={
    
    case RegexActor.RegRequest(text,pattern,classification)=>{
      val f = workerRouter ? RegexActor.RegRequest(text,pattern,classification)
      val final_result = Await.result(f, waittime)
      sender()!final_result.asInstanceOf[ArrayNode].toString()
    }
    
  }
  
  
}